 //-----------------------------------------------------------------------------
//
//Copyright(c) 2020, ThorsianWay Technologies Co, Ltd
//All rights reserved.
//
//IP Name       :   pixel_shader
//File Name     :   mul_24x32.v
//Module name   :   mul_24x32
//Full name     :   24x32 mulitplier 
//
//Author        :   zha daolu
//Email         :   
//Data          :   2020/5/13
//Version       :   V1.00
//
//Abstract      :   
//                  
//Called  by    :   GPU
//
//Modification history
//-----------------------------------------------------
//1.00: intial version 
//
//-----------------------------------------------------------------------------  
`include "marco.v"
module mul_24x32(
    clk,
    rst_n,
    mul_in_a,
    mul_in_b,
    mul_in_en,
    mul_en,
    mul_out,
    mul_out_en
);
input clk;
input rst_n;
input [23:0] mul_in_a;
input [31:0] mul_in_b;
input mul_in_en;
input mul_en;
output reg mul_out_en;
output reg [55:0] mul_out;
`ifdef SIM
reg [55:0] mul_out_pre;
reg mul_in_en_ff1;
always@(posedge clk)
begin
    if(mul_in_en && mul_en)
        mul_out_pre <=  mul_in_a * mul_in_b;
end

always@(posedge clk)
begin
    if(mul_en)
        mul_in_en_ff1 <=  mul_in_en;
end

always@(posedge clk)
begin
    if(mul_in_en_ff1 && mul_en)
        mul_out_en <=  1;
    else
        mul_out_en <=  0;
end

always@(posedge clk)
begin
    if(mul_in_en_ff1 && mul_en)
        mul_out <=  mul_out_pre;
end
`elsif ASIC
    initial
    begin
       $display("%m, empty module\n"); 
    end
`else
    initial
     begin
        $display("%m, empty module\n"); 
     end  
`endif
endmodule

