//-----------------------------------------------------------------------------
//
//Copyright(c) 2020, ThorsianWay Technologies Co, Ltd
//All rights reserved.
//
//IP Name       :   raster
//File Name     :   interpolator_float.v
//Module name   :   interpolator_float
//Full name     :   float number interpolator
//
//Author        :   zha daolu
//Email         :   
//Data          :   2020/6/1
//Version       :   V1.00
//
//Abstract      :   
//                  
//Called  by    :   GPU
//
//Modification history
//-----------------------------------------------------
//1.00: intial version 
//
//-----------------------------------------------------------------------------

//-----------------------------
//DEFINE MACRO
//-----------------------------  
module interpolator_float(
    input clk,
    input rst_n,
    input busy,
    input int_en,
    input [31:0] a0,
    input [31:0] k1,
    input [31:0] k2,
    input [31:0] d_a1,
    input [31:0] d_a2,
    output [31:0] a_out,
    output out_valid
);
//a_out = a0 + k1 * d_a1 + k2 * d_a2

wire [31:0] da1k1;
wire [31:0] da2k2;
wire valid_inter;
wire [31:0] inter_sum;

reg [31:0] a0_ff1;
reg [31:0] a0_ff2;
reg [31:0] a0_ff3;
reg [31:0] a0_ff4; 

float_mul u_da1xk1(
    .a(d_a1),
    .b(k1),
    .z(da1k1)
);  

float_mul u_da2xk1(
    .a(d_a2),
    .b(k2),
    .z(da2k2)
);

//4 cycles
float_add u_float_add1(
    .clk(clk),
    .rst_n(rst_n),
    .a(da1k1),
    .b(da2k2),
    .add_en(int_en),
    .busy(busy),
    .z(inter_sum),
    .valid(valid_inter)
); 

always@(posedge clk or negedge rst_n)
begin
    if(~rst_n)
    begin
        a0_ff1 <= 32'b0;
        a0_ff2 <= 32'b0;
        a0_ff3 <= 32'b0;
        a0_ff4 <= 32'b0; 
    end
    else if(~busy)
    begin
        a0_ff1 <= a0;
        a0_ff2 <= a0_ff1;
        a0_ff3 <= a0_ff2;
        a0_ff4 <= a0_ff3;
    end
end

//4 cycles
float_add u_float_add_2(
    .clk(clk),
    .rst_n(rst_n),
    .a(a0_ff4),
    .b(inter_sum),
    .add_en(valid_inter),
    .busy(busy),
    .z(a_out),
    .valid(out_valid)
);  
endmodule

