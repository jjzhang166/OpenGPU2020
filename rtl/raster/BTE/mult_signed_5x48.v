module mult_signed_5x48(
    input signed [5:0] a,
    input signed [47:0] b,
	output [53:0] out
);

assign out = ($signed(a)) * ($signed(b));

endmodule
