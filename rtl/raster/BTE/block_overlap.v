//-----------------------------------------------------------------------------
//
//Copyright(c) 2020, ThorsianWay Technologies Co, Ltd
//All rights reserved.
//
//IP Name       :   raster
//File Name     :   block_overlap.v
//Module name   :   block ocerlap
//Full name     :   block overlap
//
//Author        :   zha daolu
//Email         :   
//Data          :   2020/6/1
//Version       :   V1.00
//
//Abstract      :   
//                  
//Called  by    :   GPU
//
//Modification history
//-----------------------------------------------------
//1.00: intial version 
//
//-----------------------------------------------------------------------------

//-----------------------------
//DEFINE MACRO
//-----------------------------     
module block_overlap
(
    input					clk,                  //input clock                            
    input					rst_n,                //input reset, low active                
    input					busy,                 //input next stage busy                  
    input[5:0]				position_in,          //input block code                       
    input[1:0]				scale_in,             //input block size                     
    input[4:0]				lin_x0,               //input x coordinate maximize ee0 in this block                    
    input[4:0]				lin_y0,               //input y coordinate maximize ee0 in this block  
    input[4:0]				lin_x1,               //input x coordinate maximize ee1 in this block  
    input[4:0]				lin_y1,               //input y coordinate maximize ee1 in this block  
    input[4:0]				lin_x2,               //input x coordinate maximize ee2 in this block  
    input[4:0]				lin_y2,               //input y coordinate maximize ee2 in this block  
    input					xy_en,                //input block valid
    input signed[47:0]		a0,                   //input ee0 parameter
    input signed[47:0]		b0,                   //input ee0 parameter  
    input signed[47:0]		a1,                   //input ee1 parameter  
    input signed[47:0]		b1,                   //input ee1 parameter  
    input signed[47:0]		a2,                   //input ee2 parameter
    input signed[47:0]		b2,                   //input ee2 parameter
    input signed[95:0]		ee0_int,              //input ee0 for tile left-bottom corner
    input signed[95:0]		ee1_int,              //input ee1 for tile left-bottom corner
    input signed[95:0]		ee2_int,              //input ee2 for tile left-bottom corner
    output					overlap,              //output overlap 
    output[5:0]				position_out,         //output block code
    output[1:0]				scale_out,            //output block size
    output					block_overlap_busy    //output busy
);

//sign bit of ee0/1/2
wire 				sign0;
wire 				sign1;
wire 				sign2;

reg			xy_en_ff1;
reg			xy_en_ff2;
reg[7:0]	position_ff1;
reg[7:0]	position_ff2;
reg[1:0]	scale_ff1;
reg[1:0]	scale_ff2;

always@(posedge clk or negedge rst_n)
begin
	if(!rst_n)
		begin
			xy_en_ff1 		<= 1'b0;
			xy_en_ff2 		<= 1'b0;
			position_ff1	<= 6'b0;
			position_ff2	<= 6'b0;
			scale_ff1		<= 2'b0;
			scale_ff2       <= 2'b0;
		end
	else if(!busy)
		begin
			xy_en_ff1 		<= xy_en;
		    xy_en_ff2 		<= xy_en_ff1;
			position_ff1	<= position_in;
			position_ff2	<= position_ff1;
			scale_ff1		<= scale_in;
			scale_ff2		<= scale_ff1;
		end


end

//ee0 calculate
ee_cal_block	u_ee_cal_block0
(
    .clk(clk),
    .rst_n(rst_n),
    .busy(busy),
    .a(a0),
    .b(b0),
    .ee_int(ee0_int),
    .x(lin_x0),
    .y(lin_y0),
    .sign(sign0)
);

//ee1 calculate  
ee_cal_block	u_ee_cal_block1
(
    .clk(clk),
    .rst_n(rst_n),
    .busy(busy),
    .a(a1),
    .b(b1),
    .ee_int(ee1_int),
    .x(lin_x1),
    .y(lin_y1),
    .sign(sign1)
);

//ee2 calculate  
ee_cal_block	u_ee_cal_block2
(
    .clk(clk),
    .rst_n(rst_n),
    .busy(busy),
    .a(a2),
    .b(b2),
    .ee_int(ee2_int),
    .x(lin_x2),
    .y(lin_y2),
    .sign(sign2)
);



//block overlap when all sign bits are 0
assign 		overlap  = (!(sign0 | sign1 | sign2)) & xy_en_ff1 & !busy;   //overlap when all ee are positive
assign 		position_out = position_ff1;
assign		scale_out = scale_ff1;
assign		block_overlap_busy  = xy_en | xy_en_ff1;

endmodule


