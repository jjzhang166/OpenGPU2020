 //-----------------------------------------------------------------------------
//
//Copyright(c) 2020, ThorsianWay Technologies Co, Ltd
//All rights reserved.
//
//IP Name       :   pixel_shader
//File Name     :   mult_signed_48x48.v
//Module name   :   mult_signed_48x48_normal
//Full name     :   48 bit signed multiplier
//
//Author        :   zha daolu
//Email         :   
//Data          :   2020/5/13
//Version       :   V1.00
//
//Abstract      :   
//                  
//Called  by    :   GPU
//
//Modification history
//-----------------------------------------------------
//1.00: intial version 
//
//-----------------------------------------------------------------------------                               
module mult_signed_48x48_normal(
    input clk,
    input rst_n,
    input start,
    input signed [47:0] a,
    input signed [47:0] b,
    output [95:0] out,
    output valid
);
reg [95:0] mul_result;

reg start_ff;

always@(posedge clk or negedge rst_n)
begin
	if(!rst_n)
		mul_result <= 96'b0;
	else if(start)
		mul_result <= ($signed(a) * $signed(b));
end

always@(posedge clk or negedge rst_n)
begin
	if(!rst_n)
		start_ff <= 1'b0;
	else
		start_ff <= start;
end  

assign out = mul_result;
assign valid = start_ff;

endmodule
